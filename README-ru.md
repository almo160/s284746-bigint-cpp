# Big Integer C++
Библиотека для работы с большими целыми числами и дробями

## Автор
Евгений Иванов @almo160

## Установка
На платформах семейства Unix, запустите `make build` или просто `make` для сборки библиотеки.
На Windows, цели для Unix должны сопровождаться суффиксом `-win` (например, `build-win` — это `build` для Windows).
Если Вы не можете собрать или подключить к своему проекту разделяемую библиотеку, запустите <code>make build\_backup<code>, чтобы сформировать заголовок с полным исходным кодом (или просто скопируйте код в Ваш проект).
Скрипт установки ещё не реализован.

## Использование
Требует C++11 или новее.
Некоторые примеры использования приведены в файле `main.cpp`.
Запустите <code>make build\_test</code> для сборки и <code>make run\_test</code> для запуска примера.
Если Вы используете запасной заголовок, запустите <code>make build\_test.backup</code> и <code>make run\_test.backup</code>.

### Включение в проект
Вы можете включить заголовочный файл через include-путь как <code>#include &lt;s284746/bigint&gt;</code> для `s284746::BIGINT` или <code>#include &lt;s284746/ratio&gt;</code> для `s284746::RATIO` (добавьте флаг `-I/path/to/include/folder` к команде компиляции).
Если Вы используете разделяемую библиотеку, добавьте флаги `-L/path/to/library/folder` и `-ls284746num` к команде линковки (сборки исполняемого файла).

### Функции BIGINT
Класс `BIGINT` в пространстве имён `s284746` поддерживает вычисления с безразмерными целыми числами.
Объект класса `s284746::BIGINT` может быть построен из числа любого встроенного целочисленного и вещественного типа, а также из строки `std::string` по основанию от 2 до 36.
Арифметические и битовые операции имеют ту же семантику, что и для встроенных типов.
Логические операции не поддерживаются, потому что неявное преобразование в `bool` рискованно.

### Функции RATIO
Класс `RATIO` в пространстве имён `s284746` поддерживает дроби.

## Поддержка
Этот проект экспериментальный и должен быть полностью переписан. Все багрепорты направляйте на электронную почту `cool.ball2901@rambler.ru`.

### Нарушение обратной совместимости
Неявные конструкторы `BIGINT` из вещественных и строковых типов недоступны с версии 2024.06.

## Лицензия
```
Copyright (C) 2023–2024 Evgeny Ivanov

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
