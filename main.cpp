#include <iostream>
#include <s284746/bigint>
#include <s284746/ratio>

using s284746::BIGINT;
using s284746::RATIO;
using std::cout;
using std::endl;

int32_t main() {
	BIGINT x("11111111111111111111");
	cout << "x == " << x.to_string() << endl;
	BIGINT y("-222222222222222");
	cout << "y == " << y.to_int64() << endl;
	cout << "s = x + y" << endl; BIGINT s = x + y;
	cout << "s == " << s.to_string() << endl;
	cout << "x += y" << endl; x += y;
	cout << "y = x - y" << endl; y = x - y;
	cout << "x -= y" << endl; x -= y;
	cout << "x == " << x.to_int64() << endl;
	cout << "y == " << y.to_string() << endl;
	cout << "s = 2 + 2 * 2" << endl; s = BIGINT(2) + BIGINT(2) * BIGINT(2);
	cout << "s == " << s.to_uint64() << endl;
	cout << "x /= y % 10" << endl; x /= y % 10;
	cout << "y /= x % 10" << endl; y /= x % 10;
	cout << "x == " << x.to_int64() << endl;
	cout << "y == " << y.to_string() << endl;
	cout << "x = -1e16" << endl; x = BIGINT(-1e16);
	cout << "y = 4e20" << endl; y = BIGINT(4e20); // NOTE implicit constructor from double is disabled since 2024.06
	cout << "s = x + y" << endl; s = x + y;
	cout << "s == " << s.to_string() << endl;
	cout << "x = 1" << endl; x = 1;
	cout << "x <<= 54" << endl; x <<= 54;
	cout << "x(16) == " << x.to_string(16) << endl;
	cout << "y >>= 20" << endl; y >>= 20;
	cout << "y(5) == " << y.to_string(5) << endl;
	cout << "s = x & y" << endl; s = x & y;
	cout << "s == " << s.to_string() << endl;
	cout << "s = x | y" << endl; s = x | y;
	cout << "s == " << s.to_string() << endl;
	cout << "s = x ^ y" << endl; s = x ^ y;
	cout << "s == " << s.to_string() << endl;
	cout << "x = hcf(y, s)" << endl; x = hcf(y, s);
	cout << "y = hcf(x, s)" << endl; y = hcf(x, s);
	cout << "x == " << x.to_string() << endl;
	cout << "y == " << y.to_string() << endl;
	cout << "r = x / y" << endl; RATIO r(x, y);
	cout << "r == " << r.to_string() << endl;
	cout << "r += s" << endl; r += s;
	cout << "r == " << r.to_string() << endl;
	cout << "r == " << r.to_long_double() << endl;
	cout << "r = -33 / 5" << endl; r = RATIO(-33, 5);
	cout << "r == " << r.to_string() << endl;
	cout << "ceil(r) == " << ceil(r).to_string() << endl;
	cout << "floor(r) == " << floor(r).to_string() << endl;
	cout << "round(r) == " << round(r).to_string() << endl;
	cout << "trunc(r) == " << trunc(r).to_string() << endl;
	cout << "inv(r) == " << inv(r).to_string() << endl;
	r = 7.2;
	cout << r.to_string() << endl;
	return 0;
}
