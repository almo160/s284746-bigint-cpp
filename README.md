# Big Integer C++
Library for processing large integer and fractional numbers

## Author
Evgeny Ivanov @almo160

## Installation
On Unix, run `make build` or simply `make` to build library.
On Windows, targets for Unix should be suffixed with `-win` (e.g. `build-win` is Windows version of `build`).
If you fail to build or link shared library, run <code>make build\_backup<code> to form header with full source code (or just copy code to your project).
Installation script has not been implemented yet.

## Usage
Requires C++11 or newer.
Some usage examples are provided in file `main.cpp`.
Run <code>make build\_test</code> to build and <code>make run\_test</code> to run.
If you are using backup header, run <code>make build\_test.backup</code> and <code>make run\_test.backup</code> instead.

### Include to Project
You can include header file through include path as <code>#include &lt;s284746/bigint&gt;</code> for `s284746::BIGINT` or <code>#include &lt;s284746/ratio&gt;</code> for <code>s284746::RATIO</code> (provide flag `-I/path/to/include/folder` to your compiler).
If you are using shared library, provide flags `-L/path/to/library/folder` and `-ls284746num`.

### BIGINT features
Class `BIGINT` in namespace `s284746` provides large integers computations.
`s284746::BIGINT` number can be constructed from any built-in integer or floating-point number or `std::string` number in base from 2 to 36.
Arithmetic and bitwise operators have same semantics as for built-in integers.
Logical operations are not supported because implicit conversion into `bool` is dangerous.

### RATIO features
Class `RATIO` in namespace `s284746` provides fractions.

## Support
This project is sandbox and should be completely rewritten. All bugs can be reported via `cool.ball2901@rambler.ru`.

### Breaking backwards compatibility
Implicit constructors of `BIGINT` from floating-point and string types are disabled since 2024.06.

## License
```
Copyright (C) 2023–2024 Evgeny Ivanov

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
