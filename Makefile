.PHONY: all build build-win build_backup build_test build_test.backup build_test-win clean clean-win run_test run_test-win run_test.backup

BACKUP_INCLUDE_DIR = include-backup
CXXFLAGS = -std=c++11 -pedantic -Werror -Wall
INCLUDE_DIR = include
LIBRARY_DIR = lib
LIBRARY_NAME = s284746num
LIBRARY_PATH = $(LIBRARY_DIR)/lib$(LIBRARY_NAME).so
LIBRARY_PATH_WIN = $(WIN_DEST_DIR)/$(LIBRARY_NAME).dll
OBJECT_DIR = object
SOURCE_DIR = src
WIN_DEST_DIR = win

all: build

build: $(LIBRARY_PATH)

build-win: $(LIBRARY_PATH_WIN)

build_backup: $(BACKUP_INCLUDE_DIR)/s284746/bigint $(BACKUP_INCLUDE_DIR)/s284746/ratio

build_test: test

build_test.backup: test.backup

build_test-win: $(WIN_DEST_DIR)/test.exe

clean:
	rm -rf $(LIBRARY_DIR)/* test* *.o *.dll *.exe $(OBJECT_DIR)/* $(BACKUP_INCLUDE_DIR)/* $(WIN_DEST_DIR)/*
	mkdir -p $(BACKUP_INCLUDE_DIR)/s284746
	touch $(BACKUP_INCLUDE_DIR)/s284746/.gitkeep
	touch $(LIBRARY_DIR)/.gitkeep
	touch $(OBJECT_DIR)/.gitkeep
	touch $(WIN_DEST_DIR)/.gitkeep

clean-win:
	del /Q $(LIBRARY_DIR)\* test* *.o *.dll *.exe $(OBJECT_DIR)\* $(BACKUP_INCLUDE_DIR)\s284746\* $(WIN_DEST_DIR)\*
	echo > $(BACKUP_INCLUDE_DIR)\s284746\.gitkeep
	echo > $(LIBRARY_DIR)\.gitkeep
	echo > $(OBJECT_DIR)\.gitkeep
	echo > $(WIN_DEST_DIR)\.gitkeep

run_test: build_test
	LD_LIBRARY_PATH=$(LIBRARY_DIR) ./test

run_test-win: build_test-win
	$(WIN_DEST_DIR)\test.exe

run_test.backup: build_test.backup
	./test.backup

$(BACKUP_INCLUDE_DIR)/s284746/bigint: $(INCLUDE_DIR)/s284746/bigint $(SOURCE_DIR)/bigint.cpp
	cat $(INCLUDE_DIR)/s284746/bigint $(SOURCE_DIR)/bigint.cpp > $(BACKUP_INCLUDE_DIR)/s284746/bigint

$(BACKUP_INCLUDE_DIR)/s284746/ratio: $(INCLUDE_DIR)/s284746/ratio $(SOURCE_DIR)/ratio.cpp
	cat $(INCLUDE_DIR)/s284746/ratio $(SOURCE_DIR)/ratio.cpp > $(BACKUP_INCLUDE_DIR)/s284746/ratio

$(LIBRARY_PATH): $(OBJECT_DIR)/bigint.o $(OBJECT_DIR)/ratio.o
	$(CXX) -shared $(OBJECT_DIR)/bigint.o $(OBJECT_DIR)/ratio.o -o $(LIBRARY_PATH)

$(LIBRARY_PATH_WIN): $(OBJECT_DIR)/bigint.o $(OBJECT_DIR)/ratio.o
	$(CXX) -shared $(OBJECT_DIR)/bigint.o $(OBJECT_DIR)/ratio.o -o $(LIBRARY_PATH_WIN)

$(OBJECT_DIR)/bigint.o: $(INCLUDE_DIR)/s284746/bigint $(SOURCE_DIR)/bigint.cpp
	$(CXX) -c -fPIC $(CXXFLAGS) -I$(INCLUDE_DIR) $(SOURCE_DIR)/bigint.cpp -o $(OBJECT_DIR)/bigint.o

$(OBJECT_DIR)/ratio.o: $(INCLUDE_DIR)/s284746/ratio $(SOURCE_DIR)/ratio.cpp
	$(CXX) -c -fPIC $(CXXFLAGS) -I$(INCLUDE_DIR) $(SOURCE_DIR)/ratio.cpp -o $(OBJECT_DIR)/ratio.o

test: main.cpp $(INCLUDE_DIR)/s284746/bigint $(LIBRARY_PATH)
	$(CXX) $(CXXFLAGS) -I$(INCLUDE_DIR) main.cpp -o test -L$(LIBRARY_DIR) -l$(LIBRARY_NAME)

test.backup: main.cpp $(BACKUP_INCLUDE_DIR)/s284746/bigint $(BACKUP_INCLUDE_DIR)/s284746/ratio
	$(CXX) $(CXXFLAGS) -I$(BACKUP_INCLUDE_DIR) main.cpp -o test.backup

$(WIN_DEST_DIR)/test.exe: main.cpp $(INCLUDE_DIR)/s284746/bigint $(LIBRARY_PATH_WIN)
	$(CXX) $(CXXFLAGS) -I$(INCLUDE_DIR) main.cpp -o $(WIN_DEST_DIR)/test.exe -L$(WIN_DEST_DIR) -l$(LIBRARY_NAME)
