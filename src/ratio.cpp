/**
 * Rational C++
 * Library for processing fractions
 *
 * Copyright (C) 2023 Evgeny Ivanov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <cstddef>
#include <cstdio>
#include <s284746/ratio>

namespace s284746 {
    RATIO::RATIO() : RATIO::RATIO(0, 1) {}
    RATIO::RATIO(const BIGINT &number) : RATIO::RATIO(number, 1) {}
    RATIO::RATIO(const BIGINT &numerator, const BIGINT &denominator) : numer(numerator), denom(denominator) {
        if (denom == 0) throw RATIO_ERROR("Denominator cannot be zero");
        if (denom < 0) {
            numer = -numer;
            denom = -denom;
        }
        if (numer == 0) denom = 1;
        else {
            BIGINT factor = hcf(numer, denom);
            numer /= factor;
            denom /= factor;
        }
    }
    BIGINT RATIO::numerator() const {
        return numer;
    }
    BIGINT RATIO::denominator() const {
        return denom;
    }
    RATIO RATIO::operator+() const {
        return *this;
    }
    RATIO RATIO::operator-() const {
        RATIO res = *this;
        res.numer = -res.numer;
        return res;
    }
    RATIO RATIO::operator+(const RATIO &num) const {
        return RATIO(numer * num.denom + denom * num.numer, denom * num.denom);
    }
    RATIO RATIO::operator-(const RATIO &num) const {
        return RATIO(numer * num.denom - denom * num.numer, denom * num.denom);
    }
    RATIO RATIO::operator*(const RATIO &num) const {
        return RATIO(numer * num.numer, denom * num.denom);
    }
    RATIO RATIO::operator/(const RATIO &num) const {
        return RATIO(numer * num.denom, denom * num.numer);
    }
    RATIO RATIO::operator%(const RATIO &num) const {
        return RATIO(numer * num.denom % denom * num.numer, denom * num.denom);
    }
    RATIO& RATIO::operator+=(const RATIO &num) {
        return *this = *this + num;
    }
    RATIO& RATIO::operator-=(const RATIO &num) {
        return *this = *this - num;
    }
    RATIO& RATIO::operator*=(const RATIO &num) {
        return *this = *this * num;
    }
    RATIO& RATIO::operator/=(const RATIO &num) {
        return *this = *this / num;
    }
    RATIO& RATIO::operator%=(const RATIO &num) {
        return *this = *this % num;
    }
    RATIO& RATIO::operator++() {
        return *this += RATIO(1);
    }
    RATIO& RATIO::operator--() {
        return *this -= RATIO(1);
    }
    RATIO RATIO::operator++(int) {
        RATIO old = *this;
        ++*this;
        return old;
    }
    RATIO RATIO::operator--(int) {
        RATIO old = *this;
        --*this;
        return old;
    }
    bool RATIO::operator==(const RATIO &num) const {
        return numer == num.numer && denom == num.denom;
    }
    bool RATIO::operator!=(const RATIO &num) const {
        return !(*this == num);
    }
    bool RATIO::operator>(const RATIO &num) const {
        return numer * num.denom > denom * num.numer;
    }
    bool RATIO::operator<(const RATIO &num) const {
        return num > *this;
    }
    bool RATIO::operator>=(const RATIO &num) const {
        return !(*this < num);
    }
    bool RATIO::operator<=(const RATIO &num) const {
        return !(*this > num);
    }
    std::string RATIO::to_string() const {
        std::string res = numer.to_string();
        res += '/';
        res += denom.to_string();
        return res;
    }
    long double RATIO::to_long_double() const {
        if (numer == 0) return 0;
        BIGINT integer = numer / denom;
        BIGINT rem = numer % denom;
        int64_t offset = (1LL << 32);
        long double dint = 0;
        std::vector<int64_t> data;
        while (integer != 0) {
            data.push_back((integer % offset).to_int64());
            integer /= offset;
        }
        size_t n = data.size();
        for (size_t i = 1; i <= n; ++i) {
            dint *= offset;
            dint += data.at(n-i);
        }
        long double dfrac = 0;
        data.clear();
        for (size_t i = 0; i < 4; ++i) {
            rem *= offset;
            data.push_back((rem / denom).to_int64());
            rem %= denom;
        }
        for (size_t i = 1; i <= 4; ++i) {
            dfrac += data.at(4-i);
            dfrac /= offset;
        }
        return dint + dfrac;
    }
    std::vector<BIGINT> RATIO::to_scf() const { // simple continued fraction
        std::vector<BIGINT> res;
        RATIO copy = *this;
        while (true) {
            BIGINT elem = floor(copy);
            res.push_back(elem);
            copy -= elem;
            if (copy == 0) break;
            copy = inv(copy);
        }
        return res;
    }
    BIGINT ceil(const RATIO &frac) {
        BIGINT p = frac.numerator(), q = frac.denominator();
        BIGINT r = (q - p % q) % q;
        p += r;
        return p / q;
    }
    BIGINT floor(const RATIO &frac) {
        BIGINT p = frac.numerator(), q = frac.denominator();
        BIGINT r = p % q;
        if (r < 0) r += q;
        p -= r;
        return p / q;
    }
    BIGINT round(const RATIO &frac) {
        BIGINT p = frac.numerator(), q = frac.denominator();
        BIGINT r = p % q;
        if (r < 0) r += q;
        BIGINT s = floor(frac);
        if (r < q - r) return s; // rounding down
        if (r > q - r) return s + 1; // rounding up
        return s + s % 2; // rounding towards nearest even number
    }
    BIGINT trunc(const RATIO &frac) {
        return frac.numerator() / frac.denominator();
    }
    RATIO inv(const RATIO &frac) {
        return RATIO(frac.denominator(), frac.numerator());
    }
}
