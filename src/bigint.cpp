/**
 * Big Integer C++
 * Library for processing large integer numbers
 *
 * Copyright (C) 2023–2024 Evgeny Ivanov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <cmath>
#include <s284746/bigint>

namespace s284746 {
	constexpr int16_t BIGINT::digit(char ch) {
		return (ch >= '0' && ch <= '9') ? (ch - '0')
		: (ch >= 'A' && ch <= 'Z') ? (ch - 'A' + 10)
		: (ch >= 'a' && ch <= 'z') ? (ch - 'a' + 10)
		: -1;
	}
	constexpr char BIGINT::character(int16_t dg, bool cs) {
		return (dg < 0) ? '\0'
		: (dg < 10) ? static_cast<char>(dg + '0')
		: (dg < 36) ? static_cast<char>(dg - 10 + (cs ? 'A' : 'a'))
		: '\0';
	}
	void BIGINT::to_complementary(std::vector<uint32_t> &num) {
		size_t n = num.size();
		bool zeroes = false;
		for (size_t i = 0; i < n; ++i) {
			if (zeroes) num[i] = (1LL << 32) - 1 - num[i];
			else if (num[i]) {
				num[i] = (1LL << 32) - num[i];
				zeroes = true;
			}
		}
	}
	BIGINT::BIGINT() : BIGINT::BIGINT(0) {}
	BIGINT::BIGINT(float num) : BIGINT::BIGINT(static_cast<long double>(num)) {} // cannot be removed because overloading for float and double is ambiguous
	BIGINT::BIGINT(double num) : BIGINT::BIGINT(static_cast<long double>(num)) {}
	BIGINT::BIGINT(long double num) {
		neg = num < 0;
		if (neg) while (num <= (-(1LL << 32))) {
			int64_t tmp = fmod(num, (1LL << 32));
			tmp = -tmp;
			data.push_back(tmp);
			num /= (1LL << 32);
		} else while (num >= (1LL << 32)) {
			int64_t tmp = fmod(num, (1LL << 32));
			data.push_back(tmp);
			num /= (1LL << 32);
		}
		if (neg) num = -num;
		data.push_back(num);
	}
	BIGINT::BIGINT(const char *num) : BIGINT::BIGINT(std::string(num)) {} // cannot be removed because const char* implicitly converts into integer types not into string
	BIGINT::BIGINT(const char *num, int16_t base) : BIGINT::BIGINT(std::string(num), base) {}
	BIGINT::BIGINT(const std::string &num) : BIGINT::BIGINT(num, 10) {}
	BIGINT::BIGINT(const std::string &num, int16_t base) {
		if (base < 2 || base > 36)
			throw BIGINT_ERROR("Base must be [2,36]");
		size_t size = num.size();
		if (size == 0)
			throw BIGINT_ERROR("String is empty");
		uint64_t holdbase = 1;
		uint64_t tmphb = holdbase;
		uint8_t digits = 0;
		uint8_t tmpdgt = digits;
		uint64_t limit = 1LL << 32;
		while (tmphb <= limit) {
			holdbase = tmphb;
			digits = tmpdgt;
			tmphb *= base;
			++tmpdgt;
		}
		if (num[0] == '-') neg = true;
		size_t places = size - neg;
		if (places == 0)
			throw BIGINT_ERROR("String is just a minus");
		size_t tuples = places / digits;
		size_t extra = places % digits;
		uint32_t buffer = 0;
		for (size_t i = 0; i < extra; ++i) {
			int8_t dgt = digit(num[i+neg]);
			if (dgt >= 0 && dgt < base) {
				buffer *= base;
				buffer += dgt;
			} else throw BIGINT_ERROR("Wrong digit");
		}
		data.push_back(buffer);
		for (size_t i = 0; i < tuples; ++i) {
			uint64_t inter = 0;
			for (size_t j = 0; j < data.size(); ++j) {
				inter += static_cast<uint64_t>(data[j]) * static_cast<uint64_t>(holdbase);
				data[j] = inter % (1LL << 32);
				inter >>= 32;
			}
			if (inter > 0) data.push_back(inter);
			buffer = 0;
			for (size_t j = 0; j < digits; ++j) {
				int8_t dgt = digit(num[j+neg+extra+i*digits]);
				if (dgt >= 0 && dgt < base) {
					buffer *= base;
					buffer += dgt;
				} else throw BIGINT_ERROR("Wrong digit");
			}
			inter = static_cast<uint64_t>(data[0]) + static_cast<uint64_t>(buffer);
			data[0] = inter;
			inter >>= 32;
			for (size_t j = 1; j < data.size(); ++j) {
				inter += data[j];
				data[j] = inter;
				inter >>= 32;
				if (inter == 0) break;
			}
			if (inter > 0) data.push_back(inter);
		}
		if (data.back() == 0) neg = false;
	}
	BIGINT::BIGINT(const BIGINT &num) {
		neg = num.neg;
		if (num.data.back() == 0) neg = false;
		data = num.data;
	}
	BIGINT::~BIGINT() = default;
	BIGINT BIGINT::abs() {
		BIGINT res(*this);
		res.neg = false;
		return res;
	}
	BIGINT& BIGINT::operator=(const BIGINT &num) {
		neg = num.neg;
		if (num.data.back() == 0) neg = false;
		data = num.data;
		return *this;
	}
	BIGINT BIGINT::operator+() const {
		BIGINT res(*this);
		if (data.back() == 0) res.neg = false;
		return res;
	}
	BIGINT BIGINT::operator-() const {
		BIGINT res(*this);
		if (data.back() > 0) res.neg = !neg;
		return res;
	}
	BIGINT BIGINT::operator+(const BIGINT &num) const {
		if (neg && num.neg) return -(-*this + -num);
		if (neg) {
			if (-*this > num) return -(-*this - num);
			else return num - -*this;
		}
		if (num.neg) {
			if (*this < -num) return -(-num - *this);
			else return *this - -num;
		}
		size_t s1 = data.size();
		size_t s2 = num.data.size();
		size_t ms = s1 > s2 ? s1 : s2;
		uint64_t carry = 0;
		std::vector<uint32_t> rescont;
		for (size_t i = 0; i < ms; ++i) {
			if (i < s1) carry += data[i];
			if (i < s2) carry += num.data[i];
			rescont.push_back(carry % (1LL << 32));
			carry >>= 32;
		}
		if (carry > 0) rescont.push_back(carry);
		BIGINT res(0);
		res.data = rescont;
		return res;
	}
	BIGINT BIGINT::operator-(const BIGINT &num) const {
		if (neg || num.neg) return *this + -num;
		if (*this < num) return -(num - *this);
		size_t s1 = data.size();
		size_t s2 = num.data.size();
		bool carry = false;
		std::vector<uint32_t> rescont;
		for (size_t i = 0; i < s1; ++i) {
			int64_t acc = data[i];
			acc -= carry;
			if (i < s2) acc -= num.data[i];
			if (acc < 0) {
				acc += (1LL << 32);
				carry = true;
			} else carry = false;
			rescont.push_back(acc);
		}
		while (!rescont.empty() && rescont.back() == 0) rescont.pop_back();
		BIGINT res(0);
		if (!rescont.empty()) res.data = rescont;
		return res;
	}
	BIGINT BIGINT::operator*(const BIGINT &num) const {
		if (data.back() == 0 || num.data.back() == 0) return BIGINT(0);
		if (neg) return -*this * num;
		if (num.neg) return *this * -num;
		if (*this < num) return num * *this;
		size_t s1 = data.size();
		size_t s2 = num.data.size();
		BIGINT res(0);
		for (size_t i = 0; i < s2; ++i) {
			uint64_t mult = num.data[i];
			if (mult > 0) {
				std::vector<uint32_t> prodcont;
				for (size_t j = 0; j < i; ++j) prodcont.push_back(0);
				uint64_t acc = 0;
				for (size_t j = 0; j < s1; ++j) {
					acc += data[j] * mult;
					prodcont.push_back(acc % (1LL << 32));
					acc >>= 32;
				}
				if (acc > 0) prodcont.push_back(acc);
				BIGINT prod(0);
				prod.data = prodcont;
				res += prod;
			}
		}
		return res;
	}
	BIGINT BIGINT::operator/(const BIGINT &num) const {
		if (num.data.back() == 0) throw BIGINT_ERROR("Division by zero");
		if (data.back() == 0) return BIGINT(0);
		if (neg) return -(-*this / num);
		if (num.neg) return -(*this / -num);
		if (*this < num) return BIGINT(0);
		BIGINT factor(num);
		BIGINT copy(*this);
		size_t p = 0;
		while (factor <= copy) {
			factor *= BIGINT(1LL << 32);
			++p;
		}
		BIGINT res(0);
		for (size_t i = 0; i < p; ++i) {
			uint32_t acc = 0;
			for (size_t j = 0; j < 32; ++j) {
				size_t n = factor.data.size();
				uint64_t acc2 = 0;
				for (size_t fi = 1; fi <= n; ++fi) {
					acc2 <<= 32;
					acc2 += factor.data[n-fi];
					factor.data[n-fi] = acc2 / 2;
					acc2 %= 2;
				}
				if (factor.data.back() == 0) factor.data.pop_back();
				bool bit = copy >= factor;
				if (bit) copy -= factor;
				acc *= 2;
				acc += bit;
			}
			res *= BIGINT(1LL << 32);
			res += BIGINT(acc);
		}
		return res;
	}
	BIGINT BIGINT::operator%(const BIGINT &num) const {
		if (num.data.back() == 0) throw BIGINT_ERROR("Division by zero");
		if (data.back() == 0) return BIGINT(0);
		if (num.neg) return *this % -num;
		if (neg) return -(-*this % num);
		if (*this < num) return BIGINT(*this);
		BIGINT factor(num);
		BIGINT res(*this);
		size_t p = 0;
		while (factor <= res) {
			factor *= BIGINT(1LL << 32);
			++p;
		}
		for (size_t i = 0; i < p; ++i) {
			for (size_t j = 0; j < 32; ++j) {
				size_t n = factor.data.size();
				uint64_t acc = 0;
				for (size_t fi = 1; fi <= n; ++fi) {
					acc <<= 32;
					acc += factor.data[n-fi];
					factor.data[n-fi] = acc / 2;
					acc %= 2;
				}
				if (factor.data.back() == 0) factor.data.pop_back();
				if (res >= factor) res -= factor;
			}
		}
		return res;
	}
	BIGINT BIGINT::operator~() const {
		return -*this - 1;
	}
	BIGINT BIGINT::operator>>(const BIGINT &num) const {
		if (data.back() == 0 || num.data.back() == 0) return *this;
		if (num < 0) return *this << -num;
		BIGINT div = num / 32;
		std::vector<uint32_t> d;
		for (BIGINT i = 0; i < div; ++i) d.push_back(0);
		BIGINT mod = num % 32;
		d.push_back(1 << mod.data[0]);
		BIGINT x = *this;
		BIGINT y;
		y.data = d;
		return x / y;
	}
	BIGINT BIGINT::operator<<(const BIGINT &num) const {
		if (data.back() == 0 || num.data.back() == 0) return *this;
		if (num < 0) return *this >> -num;
		BIGINT div = num / 32;
		std::vector<uint32_t> m;
		for (BIGINT i = 0; i < div; ++i) m.push_back(0);
		BIGINT mod = num % 32;
		m.push_back(1 << mod.data[0]);
		BIGINT x = *this;
		BIGINT y;
		y.data = m;
		return x * y;
	}
	BIGINT BIGINT::operator&(const BIGINT &num) const {
		std::vector<uint32_t> data1 = data;
		std::vector<uint32_t> data2 = num.data;
		while (data1.size() > data2.size()) data2.push_back(0);
		while (data1.size() < data2.size()) data1.push_back(0);
		if (neg) to_complementary(data1);
		if (num.neg) to_complementary(data2);
		for (size_t i = 0; i < data1.size(); ++i) data1[i] &= data2[i];
		if (neg && num.neg) to_complementary(data1);
		while (data1.size() > 1 && data1.back() == 0) data1.pop_back();
		BIGINT res;
		res.data = data1;
		res.neg = neg && num.neg;
		return res;
	}
	BIGINT BIGINT::operator|(const BIGINT &num) const {
		std::vector<uint32_t> data1 = data;
		std::vector<uint32_t> data2 = num.data;
		while (data1.size() > data2.size()) data2.push_back(0);
		while (data1.size() < data2.size()) data1.push_back(0);
		if (neg) to_complementary(data1);
		if (num.neg) to_complementary(data2);
		for (size_t i = 0; i < data1.size(); ++i) data1[i] |= data2[i];
		if (neg || num.neg) to_complementary(data1);
		while (data1.size() > 1 && data1.back() == 0) data1.pop_back();
		BIGINT res;
		res.data = data1;
		res.neg = neg || num.neg;
		return res;
	}
	BIGINT BIGINT::operator^(const BIGINT &num) const {
		std::vector<uint32_t> data1 = data;
		std::vector<uint32_t> data2 = num.data;
		while (data1.size() > data2.size()) data2.push_back(0);
		while (data1.size() < data2.size()) data1.push_back(0);
		if (neg) to_complementary(data1);
		if (num.neg) to_complementary(data2);
		for (size_t i = 0; i < data1.size(); ++i) data1[i] ^= data2[i];
		if (neg != num.neg) to_complementary(data1);
		while (data1.size() > 1 && data1.back() == 0) data1.pop_back();
		BIGINT res;
		res.data = data1;
		res.neg = (neg != num.neg);
		return res;
	}
	BIGINT& BIGINT::operator+=(const BIGINT &num) {
		return *this = *this + num;
	}
	BIGINT& BIGINT::operator-=(const BIGINT &num) {
		return *this = *this - num;
	}
	BIGINT& BIGINT::operator*=(const BIGINT &num) {
		return *this = *this * num;
	}
	BIGINT& BIGINT::operator/=(const BIGINT &num) {
		return *this = *this / num;
	}
	BIGINT& BIGINT::operator%=(const BIGINT &num) {
		return *this = *this % num;
	}
	BIGINT& BIGINT::operator++() {
		return *this += BIGINT(1);
	}
	BIGINT& BIGINT::operator--() {
		return *this -= BIGINT(1);
	}
	BIGINT BIGINT::operator++(int) {
		BIGINT old(*this);
		++*this;
		return old;
	}
	BIGINT BIGINT::operator--(int) {
		BIGINT old(*this);
		--*this;
		return old;
	}
	BIGINT& BIGINT::operator>>=(const BIGINT &num) {
		return *this = *this >> num;
	}
	BIGINT& BIGINT::operator<<=(const BIGINT &num) {
		return *this = *this << num;
	}
	BIGINT& BIGINT::operator&=(const BIGINT &num) {
		return *this = *this & num;
	}
	BIGINT& BIGINT::operator|=(const BIGINT &num) {
		return *this = *this | num;
	}
	BIGINT& BIGINT::operator^=(const BIGINT &num) {
		return *this = *this ^ num;
	}
	bool BIGINT::operator==(const BIGINT &num) const {
		if (data.back() == 0 && num.data.back() == 0) return true;
		if (neg != num.neg) return false;
		return data == num.data;
	}
	bool BIGINT::operator!=(const BIGINT &num) const {
		return !(*this == num);
	}
	bool BIGINT::operator>(const BIGINT &num) const {
		if (data.back() == 0) return num.neg;
		if (num.data.back() == 0) return !neg && data.back() > 0;
		if (neg && !num.neg) return false;
		if (!neg && num.neg) return true;
		if (neg) return -num > -*this;
		if (data.size() > num.data.size()) return true;
		if (data.size() < num.data.size()) return false;
		size_t n = data.size();
		for (size_t i = 1; i <= n; ++i) {
			if (data[n-i] > num.data[n-i]) return true;
			if (data[n-i] < num.data[n-i]) return false;
		}
		return false;
	}
	bool BIGINT::operator<(const BIGINT &num) const {
		return num > *this;
	}
	bool BIGINT::operator>=(const BIGINT &num) const {
		return !(*this < num);
	}
	bool BIGINT::operator<=(const BIGINT &num) const {
		return !(*this > num);
	}
	int64_t BIGINT::to_int64(bool overflow/* = false*/) const {
		if (data.size() == 1) {
			int64_t res = data[0];
			if (neg) res = -res;
			return res;
		}
		if (overflow) {
			int64_t res = data[1];
			res <<= 32;
			res += data[0];
			if (neg) res = -res;
			return res;
		}
		if (data.size() > 2) throw BIGINT_ERROR("Overflow");
		if (neg) {
			int64_t res = data[1];
			if (res > (1LL << 31)) throw BIGINT_ERROR("Overflow");
			if (res == (1LL << 31) && data[0] > 0) throw BIGINT_ERROR("Overflow");
			res = -res;
			res <<= 32;
			res -= data[0];
			return res;
		}
		int64_t res = data[1];
		if (res >= (1LL << 31)) throw BIGINT_ERROR("Overflow");
		res <<= 32;
		res += data[0];
		return res;
	}
	std::string BIGINT::to_string(int16_t base/* = 10*/) const {
		if (base < 2 || base > 36)
			throw BIGINT_ERROR("Base must be [2,36]");
		if (data.back() == 0) return "0";
		uint64_t holdbase = 1;
		uint64_t tmphb = holdbase;
		uint8_t digits = 0;
		uint8_t tmpdgt = digits;
		uint64_t limit = 1LL << 32;
		while (tmphb <= limit) {
			holdbase = tmphb;
			digits = tmpdgt;
			tmphb *= base;
			++tmpdgt;
		}
		std::vector<uint32_t> tmpdata(data);
		std::string res = "";
		while (!tmpdata.empty()) {
			uint64_t acc = 0;
			size_t n = tmpdata.size();
			for (size_t i = 1; i <= n; ++i) {
				acc <<= 32;
				acc += tmpdata[n-i];
				tmpdata[n-i] = acc / holdbase;
				acc %= holdbase;
			}
			while (!tmpdata.empty() && tmpdata.back() == 0) tmpdata.pop_back();
			std::string tmpstr = "";
			for (size_t i = 0; i < digits; ++i) {
				if (acc > 0) {
					tmpstr += character(acc % base, false);
					acc /= base;
				} else if (!tmpdata.empty()) tmpstr += '0';
			}
			size_t s = tmpstr.size();
			for (size_t i = 0; i < s / 2; ++i) {
				char t = tmpstr[i];
				tmpstr[i] = tmpstr[s-i-1];
				tmpstr[s-i-1] = t;
			}
			res = tmpstr + res;
		}
		if (neg) res = '-' + res;
		return res;
	}
	uint64_t BIGINT::to_uint64(bool overflow/* = false*/) const {
		if (overflow) {
			uint64_t res = 0;
			if (data.size() == 1) res = data[0];
			else {
				res = data[1];
				res <<= 32;
				res += data[0];
			}
			if (neg) res = -res;
			return res;
		}
		if (neg) throw BIGINT_ERROR("Overflow");
		if (data.size() == 1) return data[0];
		if (data.size() > 2) throw BIGINT_ERROR("Overflow");
		uint64_t res = data[1];
		res <<= 32;
		res += data[0];
		return res;
	}
	BIGINT hcf(const BIGINT &x, const BIGINT &y) {
		if (x == 0 || y == 0) return 0;
		if (x < 0) return hcf(-x, y);
		if (y < 0) return hcf(x, -y);
		BIGINT a = x, b = y;
		while (a > 0 && b > 0) {
			if (a > b) a %= b;
			else b %= a;
		}
		return a > 0 ? a : b;
	}
}
