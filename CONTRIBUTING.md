# Contributing Guide
## Merge request
Changes are merged into <code>devel</code> branch first. Do not create merge requests into <code>main</code>!
